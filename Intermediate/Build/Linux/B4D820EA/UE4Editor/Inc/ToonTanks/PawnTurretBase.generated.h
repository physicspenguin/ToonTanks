// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOONTANKS_PawnTurretBase_generated_h
#error "PawnTurretBase.generated.h already included, missing '#pragma once' in PawnTurretBase.h"
#endif
#define TOONTANKS_PawnTurretBase_generated_h

#define ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_SPARSE_DATA
#define ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_RPC_WRAPPERS
#define ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPawnTurretBase(); \
	friend struct Z_Construct_UClass_APawnTurretBase_Statics; \
public: \
	DECLARE_CLASS(APawnTurretBase, APawnBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ToonTanks"), NO_API) \
	DECLARE_SERIALIZER(APawnTurretBase)


#define ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPawnTurretBase(); \
	friend struct Z_Construct_UClass_APawnTurretBase_Statics; \
public: \
	DECLARE_CLASS(APawnTurretBase, APawnBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ToonTanks"), NO_API) \
	DECLARE_SERIALIZER(APawnTurretBase)


#define ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APawnTurretBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APawnTurretBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APawnTurretBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APawnTurretBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APawnTurretBase(APawnTurretBase&&); \
	NO_API APawnTurretBase(const APawnTurretBase&); \
public:


#define ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APawnTurretBase() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APawnTurretBase(APawnTurretBase&&); \
	NO_API APawnTurretBase(const APawnTurretBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APawnTurretBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APawnTurretBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APawnTurretBase)


#define ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FireRate() { return STRUCT_OFFSET(APawnTurretBase, FireRate); } \
	FORCEINLINE static uint32 __PPO__FireRange() { return STRUCT_OFFSET(APawnTurretBase, FireRange); } \
	FORCEINLINE static uint32 __PPO__AimRange() { return STRUCT_OFFSET(APawnTurretBase, AimRange); } \
	FORCEINLINE static uint32 __PPO__ScoreValue() { return STRUCT_OFFSET(APawnTurretBase, ScoreValue); }


#define ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_12_PROLOG
#define ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_SPARSE_DATA \
	ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_RPC_WRAPPERS \
	ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_INCLASS \
	ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_PRIVATE_PROPERTY_OFFSET \
	ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_SPARSE_DATA \
	ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_INCLASS_NO_PURE_DECLS \
	ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOONTANKS_API UClass* StaticClass<class APawnTurretBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ToonTanks_Source_ToonTanks_Pawns_PawnTurretBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
