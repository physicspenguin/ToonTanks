// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ToonTanks/Pawns/PawnTurretDoubleBarrel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePawnTurretDoubleBarrel() {}
// Cross Module References
	TOONTANKS_API UClass* Z_Construct_UClass_APawnTurretDoubleBarrel_NoRegister();
	TOONTANKS_API UClass* Z_Construct_UClass_APawnTurretDoubleBarrel();
	TOONTANKS_API UClass* Z_Construct_UClass_APawnTurretBase();
	UPackage* Z_Construct_UPackage__Script_ToonTanks();
// End Cross Module References
	void APawnTurretDoubleBarrel::StaticRegisterNativesAPawnTurretDoubleBarrel()
	{
	}
	UClass* Z_Construct_UClass_APawnTurretDoubleBarrel_NoRegister()
	{
		return APawnTurretDoubleBarrel::StaticClass();
	}
	struct Z_Construct_UClass_APawnTurretDoubleBarrel_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APawnTurretDoubleBarrel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawnTurretBase,
		(UObject* (*)())Z_Construct_UPackage__Script_ToonTanks,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APawnTurretDoubleBarrel_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Pawns/PawnTurretDoubleBarrel.h" },
		{ "ModuleRelativePath", "Pawns/PawnTurretDoubleBarrel.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APawnTurretDoubleBarrel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APawnTurretDoubleBarrel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APawnTurretDoubleBarrel_Statics::ClassParams = {
		&APawnTurretDoubleBarrel::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APawnTurretDoubleBarrel_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APawnTurretDoubleBarrel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APawnTurretDoubleBarrel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APawnTurretDoubleBarrel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APawnTurretDoubleBarrel, 1687514155);
	template<> TOONTANKS_API UClass* StaticClass<APawnTurretDoubleBarrel>()
	{
		return APawnTurretDoubleBarrel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APawnTurretDoubleBarrel(Z_Construct_UClass_APawnTurretDoubleBarrel, &APawnTurretDoubleBarrel::StaticClass, TEXT("/Script/ToonTanks"), TEXT("APawnTurretDoubleBarrel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APawnTurretDoubleBarrel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
