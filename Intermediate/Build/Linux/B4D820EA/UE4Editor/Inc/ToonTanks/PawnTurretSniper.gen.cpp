// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ToonTanks/Pawns/PawnTurretSniper.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePawnTurretSniper() {}
// Cross Module References
	TOONTANKS_API UClass* Z_Construct_UClass_APawnTurretSniper_NoRegister();
	TOONTANKS_API UClass* Z_Construct_UClass_APawnTurretSniper();
	TOONTANKS_API UClass* Z_Construct_UClass_APawnTurretBase();
	UPackage* Z_Construct_UPackage__Script_ToonTanks();
// End Cross Module References
	void APawnTurretSniper::StaticRegisterNativesAPawnTurretSniper()
	{
	}
	UClass* Z_Construct_UClass_APawnTurretSniper_NoRegister()
	{
		return APawnTurretSniper::StaticClass();
	}
	struct Z_Construct_UClass_APawnTurretSniper_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APawnTurretSniper_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawnTurretBase,
		(UObject* (*)())Z_Construct_UPackage__Script_ToonTanks,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APawnTurretSniper_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Pawns/PawnTurretSniper.h" },
		{ "ModuleRelativePath", "Pawns/PawnTurretSniper.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APawnTurretSniper_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APawnTurretSniper>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APawnTurretSniper_Statics::ClassParams = {
		&APawnTurretSniper::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APawnTurretSniper_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APawnTurretSniper_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APawnTurretSniper()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APawnTurretSniper_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APawnTurretSniper, 4251891962);
	template<> TOONTANKS_API UClass* StaticClass<APawnTurretSniper>()
	{
		return APawnTurretSniper::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APawnTurretSniper(Z_Construct_UClass_APawnTurretSniper, &APawnTurretSniper::StaticClass, TEXT("/Script/ToonTanks"), TEXT("APawnTurretSniper"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APawnTurretSniper);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
