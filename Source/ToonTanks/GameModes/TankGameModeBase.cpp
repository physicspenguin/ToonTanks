// Fill out your copyright notice in the Description page of Project Settings.


#include "TankGameModeBase.h"
#include "Containers/Array.h"
#include "ToonTanks/Pawns/PawnTurret.h"
#include "ToonTanks/Pawns/PawnTank.h"
#include "ToonTanks/PlayerControllers/PlayerControllerBase.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"


void ATankGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	// Get references and Win/Loose conditions.

	// Call HandleGameStart() to initialize start countdown, turrets etc.
	HandleGameStart();

}

void ATankGameModeBase::ActorDied(AActor* DeadActor)
{
	// What actor has been killed?
	if (DeadActor == PlayerTank) {
		PlayerTank->HandleDestruction();
		HandleGameOver(false);
	} else if (APawnTurret* DestroyedTurret = Cast<APawnTurret>(DeadActor)) {
		TargetTurretNumber -= 1;
		DestroyedTurret->HandleDestruction();
		if (TargetTurretNumber <= 0) {
			HandleGameOver(true);
		}
	}
	// If Player -> Game Over
	// If turrt diminish turret count
	UE_LOG(LogTemp, Warning, TEXT("A pawn died"));

}

void ATankGameModeBase::HandleGameStart()
{
	// Initialize the start countdown
	// Activate Turrets
	TargetTurretNumber = GetTargetTurretCount();

	PlayerTank = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this, 0));

	PlayerControllerRef = Cast<APlayerControllerBase>(UGameplayStatics::GetPlayerController(this, 0));

	if (PlayerControllerRef) {
		PlayerControllerRef->SetPlayerEnabledState(false);

		FTimerHandle PlayerEnableHandle;
		FTimerDelegate PlayerEnableDelegate = FTimerDelegate::CreateUObject(PlayerControllerRef, &APlayerControllerBase::SetPlayerEnabledState, true);

		GetWorld()->GetTimerManager().SetTimer(PlayerEnableHandle, PlayerEnableDelegate, StartDelayTime, false);
	}
	// Call Blueprint GameStart()
	GameStart();

}

void ATankGameModeBase::HandleGameOver(bool PlayerWon)
{
	// See if player has destroyed all turrets, show win results
	// else if player destroyed show loose result
	// Call Blueprint of GameOver()
	if (PlayerControllerRef && !PlayerWon) {
		PlayerControllerRef->SetPlayerEnabledState(false);
	}
	GameOver(PlayerWon);
}

int32 ATankGameModeBase::GetTargetTurretCount()
{

	TArray<AActor*> TurretActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APawnTurret::StaticClass(), TurretActors);
	return TurretActors.Num();

}
