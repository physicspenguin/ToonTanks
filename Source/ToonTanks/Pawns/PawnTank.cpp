// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnTank.h"

#include "Engine/EngineBaseTypes.h"
#include "GameFramework/SpringArmComponent.h"
#include "ToonTanks/GameModes/TankGameModeBase.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "ToonTanks/PlayerControllers/PlayerControllerBase.h"

// Constructor
APawnTank::APawnTank()
{
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm"));
	SpringArm->SetupAttachment(RootComponent);

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm);
}

// Called when the game starts or when spawned
void APawnTank::BeginPlay()
{
	Super::BeginPlay();
	PlayerControllerRef = Cast<APlayerControllerBase>(GetController());
}


// Called every frame
void APawnTank::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Split into extra functions to be able to determine the order of movement
	RotateActor();
	TranslateActor();

	if (PlayerControllerRef) {
		FHitResult TraceHitResult;
		PlayerControllerRef->GetHitResultUnderCursor(ECC_Visibility,
													false,
													TraceHitResult);
		FVector HitLocation = TraceHitResult.ImpactPoint;

		RotateTurretToTarget(HitLocation);
	}
}

// Destroys player character
void APawnTank::HandleDestruction()
{
	Super::HandleDestruction();
// Set player dead
	bIsPlayerAlive = false;
// Hide Player
	SetActorHiddenInGame(true);
// Stop player tick to save performance
	SetActorTickEnabled(false);
}

// Called to bind functionality to input
void APawnTank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Tells the pawn to call calculate translation when pressing move forward
	PlayerInputComponent->BindAxis("MoveForward",
									this,
									&APawnTank::CalculateTranslationInput);

	// Tells the pawn to call calculate rotation when pressing turn
	PlayerInputComponent->BindAxis("Turn",
									this,
									&APawnTank::CalculateRotationInput);

	// Tells thse pawn when to fire
	PlayerInputComponent->BindAction("Fire",
									IE_Pressed,
									this,
									&APawnTank::Fire);
}

// Calculate the rotation of the next step
void APawnTank::CalculateRotationInput(float InputValue)
{
	// Calculate the rotation that is to be used
	FRotator RotationVector =
			FRotator(0,
					InputValue * RotationSpeed * GetWorld()->DeltaTimeSeconds,
					0);
	// Convert the rotation into quaternions
	RotationDirection = FQuat(RotationVector);
}

// Calculate the translation of the next step
void APawnTank::CalculateTranslationInput(float InputValue)
{
	// Calculate the translation vector
	TranslationDirection =
		FVector(InputValue * TranslationSpeed * GetWorld()->DeltaTimeSeconds,
				0,
				0);
}

// Apply rotation to actor
void APawnTank::RotateActor()
{
	// Add the rotation to current Actor rotation
	AddActorLocalRotation(RotationDirection, true);

}

// Apply translation to actor
void APawnTank::TranslateActor()
{
	// Add the translation Vector to the current actor position
	AddActorLocalOffset(TranslationDirection, true);
}

// Output whether player lives or not
bool APawnTank::GetIsPlayerAlive()
{
	return bIsPlayerAlive;
}
