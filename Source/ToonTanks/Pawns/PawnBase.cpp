// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnBase.h"

#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Math/Vector.h"
#include "Templates/Casts.h"
#include "ToonTanks/Actors/ProjectileBase.h"
#include "ToonTanks/Components/HealthComponent.h"



// Sets default values
APawnBase::APawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Collider"));
	RootComponent = CapsuleComponent;

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh"));
	BaseMesh->SetupAttachment(RootComponent);

	TurretMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Turret Mesh"));
	TurretMesh->SetupAttachment(BaseMesh);

	ProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Projectile Spawn Point"));
	ProjectileSpawnPoint->SetupAttachment(TurretMesh);

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
}

void APawnBase::RotateTurretToTarget(FVector VectorToTarget)
{
	// Reduce look vector to pure rotation
	FVector RotationVector = FVector(VectorToTarget.X,
									VectorToTarget.Y,
									TurretMesh->GetComponentLocation().Z) -
									TurretMesh->GetComponentLocation();
	FRotator Rotation = RotationVector.Rotation();
	float CurrentYaw = TurretMesh->GetComponentRotation().Yaw;
	float NextYaw = Rotation.Yaw;
	float SpeedTest = CurrentYaw - NextYaw;

	// Set Turret Rotation
	if (abs(SpeedTest) > TurretRotationSpeed && abs(SpeedTest) < 360 - TurretRotationSpeed) {
		if ( NextYaw < 0 ){
			if (SpeedTest <= 180 && SpeedTest > 0) {
				Rotation.Yaw = CurrentYaw - TurretRotationSpeed;
			} else {
				Rotation.Yaw = CurrentYaw + TurretRotationSpeed;
			}
		} else {
			if (SpeedTest >= -180 && SpeedTest < 0) {
				Rotation.Yaw = CurrentYaw + TurretRotationSpeed;
			} else {
				Rotation.Yaw = CurrentYaw - TurretRotationSpeed;
			}
		}
	}
	TurretMesh->SetWorldRotation(Rotation);
}

void APawnBase::Fire()
{
	// Get ProjectileSpawnPoint location && rotation
	// Then Spawn projectile there with velocity forward.
	UE_LOG(LogTemp, Warning, TEXT("Fire"));

	// Check if projectile exists and then spawn projectile
	if (ProjectileClass) {
		AProjectileBase* TempProjectile = GetWorld()->SpawnActor<AProjectileBase>(
				ProjectileClass,
				ProjectileSpawnPoint->GetComponentLocation(),
				ProjectileSpawnPoint->GetComponentRotation()
				);
		TempProjectile->SetOwner(this);
	}
}

void APawnBase::HandleDestruction()
{
	// --- Universal Functionality ---
	// Death effects: particles, sound, camerashake
	UGameplayStatics::SpawnEmitterAtLocation(this, DeathParticles, GetActorLocation());

	GetWorld()->GetFirstPlayerController()->ClientPlayCameraShake(ShakeExplode);
}
