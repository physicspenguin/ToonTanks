// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Math/Vector.h"
#include "Templates/SubclassOf.h"
#include "UObject/ObjectMacros.h"
#include "PawnBase.generated.h"

class UCapsuleComponent;
class AProjectileBase;
class UHealthComponent;

UCLASS()
class TOONTANKS_API APawnBase : public APawn
{
	GENERATED_BODY()

	private:

	// Componenets
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Components", meta = (AllowPrivateAccess = "true"));
	UCapsuleComponent* CapsuleComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Components", meta = (AllowPrivateAccess = "true"));
	UStaticMeshComponent* BaseMesh = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Components", meta = (AllowPrivateAccess = "true"));
	UStaticMeshComponent* TurretMesh = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Components", meta = (AllowPrivateAccess = "true"));
	USceneComponent* ProjectileSpawnPoint = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Components", meta = (AllowPrivateAccess = "true"));
	UHealthComponent* HealthComponent = nullptr;

	// Variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Combat", meta = (AllowPrivateAccess = "true"));
	float TurretRotationSpeed = 10.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Projectiles", meta = (AllowPrivateAccess = "true"));
	TSubclassOf<AProjectileBase> ProjectileClass = nullptr;

	UPROPERTY(EditAnywhere, Category="Effects");
	UParticleSystem* DeathParticles = nullptr;

	UPROPERTY(EditAnywhere, Category = "Effects")
	USoundBase* SoundDeath = nullptr;

	UPROPERTY(EditAnywhere, Category = "Effects")
	TSubclassOf<UCameraShake> ShakeExplode;

	public:
		// Sets default values for this pawn's properties
		APawnBase();

		virtual void HandleDestruction();

	// Can be seen by derived classes
	protected:
		void RotateTurretToTarget(FVector VectorToTarget);

		void Fire();


};
