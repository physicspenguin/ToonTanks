// Fill out your copyright notice in the Description page of Project Settings.

#include "Logging/LogMacros.h"
#include "Kismet/GameplayStatics.h"
#include "ToonTanks/GameInstances/MyGameInstance.h"
#include "PawnTank.h"

#include "PawnTurretBase.h"

void APawnTurretBase::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(FireRateTimer, this, &APawnTurretBase::CheckFireCondition, FireRate, true);
	PlayerPawn = Cast<APawnTank>(UGameplayStatics::GetPlayerPawn(this,0));
	CurrentGameInstance = Cast<UMyGameInstance>(GetGameInstance());
}

void APawnTurretBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CheckAimCondition();

}

void APawnTurretBase::HandleDestruction()
{
	Super::HandleDestruction();
	Destroy();
	CurrentScore = CurrentGameInstance->GetPlayerScore();
	CurrentGameInstance->SetPlayerScore(CurrentScore+ScoreValue);

}

void APawnTurretBase::CheckAimCondition()
{
	if (!PlayerPawn || GetDistanceToPlayer() > AimRange) {
		return;
	}
	RotateTurretToTarget(PlayerPawn->GetActorLocation());
}

void APawnTurretBase::CheckFireCondition()
{
	// Check if player is alive
	if (!PlayerPawn) {
		return;
	} else if ( !PlayerPawn->GetIsPlayerAlive() ){
		return;
	}
		// aim at players future position.
	// Check if player is in range
	if (GetDistanceToPlayer() <= FireRange) {
		Fire();
	}
}

float APawnTurretBase::GetDistanceToPlayer()
{
	if(!PlayerPawn){
		return 0.f;
	}
	return FVector::Dist(PlayerPawn->GetActorLocation(), GetActorLocation());
}
