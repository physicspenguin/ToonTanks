// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Math/Quat.h"
#include "Math/Vector.h"
#include "UObject/ObjectMacros.h"
#include "PawnBase.h"
#include "UObject/ObjectVersion.h"

#include "PawnTank.generated.h"

//Forward declarations of classes;
class USpringArmComponent;
class UCameraComponent;
class APlayerControllerBase;

UCLASS()
class TOONTANKS_API APawnTank : public APawnBase
{
	GENERATED_BODY()
	
	public:
		// Sets default values for this pawn's properties
		APawnTank();

		// Called every frame
		virtual void Tick(float DeltaTime) override;

		// Called to bind functionality to input
		virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

		// Handle the destruction of the player pawn
		virtual void HandleDestruction() override;

		// Output wether player is alive or not
		bool GetIsPlayerAlive();

	private:
		// Component declarations
			// Springarm for camera mounting
				UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"));
				USpringArmComponent* SpringArm = nullptr;
			// Camera
				UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"));
				UCameraComponent* Camera = nullptr;

		// Movement
			// Movement Variables
				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = (AllowPrivateAccess = "true"))
				float TranslationSpeed = 300.f;
				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = (AllowPrivateAccess = "true"))
				float RotationSpeed = 100.f;
				FVector TranslationDirection;
				FQuat RotationDirection;

			// Movement functions
				// Calculate the rotation of the next step
				void CalculateRotationInput(float InputValue);
				// Calculate the translation of the next step
				void CalculateTranslationInput(float InputValue);
				// Apply rotation to actor
				void RotateActor();
				// Apply translation to actor
				void TranslateActor();

		// Firing
			APlayerControllerBase* PlayerControllerRef = nullptr;

		bool bIsPlayerAlive = true;

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;


	
	
};
