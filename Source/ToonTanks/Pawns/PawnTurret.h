// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "PawnTurretBase.h"

#include "PawnTurret.generated.h"

class APawnTank;
class UMyGameInstance;

UCLASS()
class TOONTANKS_API APawnTurret : public APawnTurretBase
{
	GENERATED_BODY()

	//public:
		//// Called every frame
		//virtual void Tick(float DeltaTime) override;

		//virtual void HandleDestruction() override;

	//protected:
		//// Called when the game starts or when spawned
		//virtual void BeginPlay() override;


	//private:

		//void CheckFireCondition();

		//void CheckAimCondition();

		//float GetDistanceToPlayer();

		//FTimerHandle FireRateTimer;

		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
		//float FireRate = 2.0f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
		//float FireRange = 500.0f;
		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
		//float AimRange = 550.0f;

		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Scoring", meta = (AllowPrivateAccess = "true"))
		//int ScoreValue = 10;
		//int CurrentScore;

		//APawnTank* PlayerPawn = nullptr;
		//UMyGameInstance* CurrentGameInstance = nullptr;

};
