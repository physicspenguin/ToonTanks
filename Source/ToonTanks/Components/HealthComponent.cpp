// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "ToonTanks/GameModes/TankGameModeBase.h"
#include "Kismet/GameplayStatics.h"


// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// Reset Health
	CurrentHealth = DefaultHealth;

	// Get reference for the game mode
	GameModeRef = Cast<ATankGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));

	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::TakeDamage);

}


	void UHealthComponent::TakeDamage(AActor* DamageActor, float Damage,
			const UDamageType* DamageType, AController* InstigatedBy,
			AActor* DamageCauser)
{
	// Check if there is any damage to be applied
	if (Damage == 0 || CurrentHealth <= 0) {
		return;
	}
	// if yes then apply damage
	CurrentHealth = FMath::Clamp(CurrentHealth - Damage, 0.f, DefaultHealth);

	// Check if health still present
	if (CurrentHealth <= 0) {
		// check if Game Mode can be found
		if (GameModeRef) {
			// If Game Mode found then kill actor
			GameModeRef->ActorDied(GetOwner());
		} else {
			// else warn me please!
			UE_LOG(LogTemp, Warning, TEXT("HealthComponent has no valid reference to GameMode"));
		}
	}
}
