// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Templates/SubclassOf.h"
#include "ProjectileBase.generated.h"

class UProjectileMovementComponent;
class UParticleSystemComponent;

UCLASS()
class TOONTANKS_API AProjectileBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectileBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* ProjectileMovement = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* ProjectileMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UDamageType> DamageType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	float ProjectileSpeed = 1200.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	float ProjectileDamage = 50.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Misc", meta = (AllowPrivateAccess = "true"))
	float ProjectileLifespan = 3.f;

// Effects
	UPROPERTY(EditAnywhere, Category = "Effects")
	UParticleSystem* CollisionParticles = nullptr;

	UPROPERTY(EditAnywhere, Category = "Effects")
	UParticleSystemComponent* ParticleTrailComponent = nullptr;

	UPROPERTY(EditAnywhere, Category = "Effects")
	USoundBase* SoundHit = nullptr;
	UPROPERTY(EditAnywhere, Category = "Effects")
	USoundBase* SoundLaunch = nullptr;
	UPROPERTY(EditAnywhere, Category = "Effects")
	TSubclassOf<UCameraShake> ShakeHit;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

};
