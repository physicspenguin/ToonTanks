// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"

UCLASS()
class TOONTANKS_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()

	private:
		int PlayerScore = 0;

	public:
		UFUNCTION(BlueprintCallable)
		int GetPlayerScore();

		UFUNCTION(BlueprintCallable)
		void SetPlayerScore(int NewScore);


};
